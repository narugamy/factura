<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Errores extends CI_Controller{

 	function __construct(){
		 parent::__construct();
	}

	function Index(){
		$error=['heading'=>'404','message'=>'Archivo no encontrado o no tiene acceso a el'];
		$this->load->view('errors/html/error_404',$error);
	}

}