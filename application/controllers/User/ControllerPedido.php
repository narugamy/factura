	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class ControllerPedido extends CI_Controller{

		private $perfil;

		public function __construct(){
			parent::__construct();
			$this->load->model('Modelorden');
			$this->load->model('Modelproducto');
			$this->perfil=$this->session->userdata('Perfil');
			if($this->perfil=='vista' || empty($this->perfil)){
				redirect(base_url());
			}
		}

		function Fechas($array){
			$array['created_at']=date('Y-m-d H:i:s');
			$array['updated_at']=date('Y-m-d H:i:s');
			return $array;
		}

		public function Guardar(){
			$stado=$this->input->post(null,true);
			if(!empty($stado['stado'])) {
				$carrito = $this->session->userdata('carrito');
				$cantidad = count($carrito);
				if ($cantidad > 0) {
					$orden['user_id'] = $this->session->userdata('id');
					$orden['num'] = $this->Modelorden->getmax()->num + 1;
					if (!is_numeric($orden['num'])) {
						$orden['num'] = 1;
					}
					$total = 0;
					for ($i = 0; $i < $cantidad; $i++) {
						$total += $carrito[$i]['price'];
					}
					unset($carrito);
					$carrito=array();
					$this->session->set_userdata('carrito',$carrito);
					$orden['total'] = $total;
					$orden = $this->Fechas($orden);
					$id = $this->Modelorden->insert($orden);
					$this->saveInner($id);
					$alert['alert'] = "Orden hecha exitosamente con el numero: " . $id . "";
					$alert['alertc'] = "alert alert-success alert-dismissible";
					$pedido = $this->Modelorden->get(array('id' => $id));
					$array = ['vista' => 'Vista', 'alert' => $alert, 'pedido' => $pedido];
						$this->load->view("user/carrito/" . $array['vista'], $array);
				}else{
					$alert['alert'] = "No se puede hacer ninguna transaccion porque no existe ningun producto";
					$alert['alertc'] = "alert alert-success alert-dismissible";
					$array = ['alert' => $alert, 'vista' => 'Index'];
					$this->load->view("user/carrito/" . $array['vista'], $array);
				}
			}else{
				redirect('404_override');
			}
		}

		public function Index(){
			$carrito=$this->session->userdata('carrito');
			$alert=$this->input->post(null,true);
			$titulo=['title'=>'Panel de Administrador de Usuarios'];
			$array=['vista'=>'Index','alert'=>$alert,'products'=>$carrito];
			if(!empty($alert['stado'])){
				$this->load->view("user/carrito/".$array['vista'],$array);
			}else{
				$this->Vista($array,$titulo);
			}
		}

		public function Vista($array,$titulo){
			$this->load->view('user/Template/Header',$titulo);
			$this->load->view('user/carrito/'.$array['vista'],$array);
			$this->load->view('user/Template/Footer');
		}

		public function saveInner($id){
			$carrito=$this->session->userdata('carrito');
			$cantidad=count($carrito);
			for($i=0;$i<$cantidad;$i++){
				$producto=['order_id'=>$id,'product_id'=>$carrito[$i]['id'],'number'=>$carrito[$i]['number'],'price'=>$carrito[$i]['price']];
				$producto=$this->Fechas($producto);
				$this->Modelorden->insertinner($producto);
			}
		}

	}

