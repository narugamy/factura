
<!DOCTYPE html>
<html lang="es">
<meta http-equiv="content-type" charset="utf-8" />
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Error 404 - Página no Encontrada</title>

	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/library/Bootstrap/css/bootstrap.min.css">

	<link href="<?= base_url()?>assets/css/stylesheet.css" rel="stylesheet">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,900,300,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ultra' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>


</head>
<body class="dusk">
<div class="col-md-12">
	<div class="owl-background">
		<div class="moon"><div class="left">4</div><div class="right">4</div></div>
		<div class="owl">
			<div class="wing1"></div>
			<div class="wing2"></div>
			<div class="wing3"></div>
			<div class="wing4"></div>
			<div class="wing5"></div>
			<div class="wing6"></div>
			<div class="wing7"></div>
			<div class="wing8"></div>
			<div class="wing9"></div>
			<div class="wing10"></div>
			<div class="wing11"></div>
			<div class="wing12"></div>
			<div class="owl-head">
				<div class="ears"></div>
			</div>
			<div class="owl-body">
				<div class="owl-eyes">
					<div class="owleye">
						<div class="owleye inner"></div>
						<div class="owleye inner inner-2"></div>
						<div class="eyelid top"></div>
						<div class="eyelid bottom"></div>
					</div>
					<div class="owleye">
						<div class="owleye inner"></div>
						<div class="owleye inner inner-2"></div>
						<div class="eyelid top"></div>
						<div class="eyelid bottom"></div>
					</div>
					<div class="nose"></div>
				</div>
				<div class="feet">
					<div class="foot1"></div>
					<div class="foot2"></div>
				</div>
			</div>
			<div class="branch"></div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="message">
		<h2>Página no Encontrada</h2>
		<p>La Página que estás buscando no existe !</p><br>
		<div class="btndiv"><a href="<?=base_url()?>"><button class="btnpop">Página Principal</button><a></div>
	</div>
</div>
<div id='stars1'></div>
<div id='stars2'></div>
<div id='stars3'></div>
<div id='sstar'></div>
</body>
</html>
