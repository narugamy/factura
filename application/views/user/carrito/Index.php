<div class="banner">
	<h2>
		<a href="<?=base_url()?>paneluser" title="index">Home</a>
		<i class="fa fa-angle-right"></i>
		<span>Carrito</span>

	</h2>
</div>
<div class="centro">
	<?php if(!empty($alert['alert'])){?>
		<div class="<?=$alert['alertc']?>">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><?=$alert['alert']?></p>
		</div>
	<?php } ?>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<th>Nombre</th>
				<th>Unidades</th>
				<th>Precio unitario</th>
				<th>Precio total</th>
				<th>Acciones</th>
			</thead>
			<tbody>
				<?php if(!empty($products)){?>
					<?php foreach($products as $product){?>
						<tr>
							<td><?=$product['name']?></td>
							<td>
								<?=form_open(base_url()."paneluser/carrito/update",['class'=>'form','id'=>'form'])?>
									<?=form_input(['class'=>'form-control','placeholder'=>'Cantidad','id'=>'number','name'=>'number','type'=>'number','min'=>'0','step'=>'1','value'=>$product['number']])?>
									<?= form_input(['name'=>'id','type'=>'hidden','value'=>$product['id']])?>
									<button class="btn btn-warning" type="submit">Update</button>
								<?=form_close()?>
							</td>
							<td><?=($product['price']/$product['number'])?></td>
							<td><?=$product['price']?></td>
							<td>
								<a href="<?=base_url()?>paneluser/carrito/delete/<?=$product['id']?>" class="btn btn-danger boton"><span class="fa fa-times" aria-hidden="true"></span></a>
							</td>
						</tr>
					<?php }?>
				<?php }?>
			</tbody>
		</table>
		<div>
			<a href="<?=base_url()?>paneluser/carrito/pedido" class="btn btn-success boton2" id="json">Enviar Pedido</a>
		</div>
	</div>
</div>