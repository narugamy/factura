<div class="banner">
	<h2>
		<a href="<?=base_url()?>paneladmin" title="index">Home</a>
		<i class="fa fa-angle-right"></i>
		<span>Carrito</span>

	</h2>
</div>
<div class="centro">
	<?php if(!empty($alert['alert'])){?>
		<div class="<?=$alert['alertc']?>">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><?=$alert['alert']?></p>
		</div>
	<?php } ?>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
			<th>Nombre</th>
			<th>Precio</th>
			<th>Stock</th>
			<th>Acciones</th>
			</thead>
			<tbody>
			<?php if(!empty($productos)){ ?>
				<?php foreach($productos as $product){ ?>
					<tr>
						<td><?=$product->name?></td>
						<td><?=$product->price?></td>
						<td><?=$product->stock?>
						</td>
						<td>
							<?=form_open(base_url()."paneladmin/product/update",['class'=>'form2','id'=>'form'])?>
								<?=form_input(['name'=>'id','type'=>'hidden','value'=>$product->id,'class'=>'form-control'])?>
								<button class="btn btn-warning" type="submit">Update</button>
							<?=form_close()?>
							<a href="<?=base_url()?>paneladmin/product/delete/<?=$product->id?>" class="btn btn-danger boton"><span class="fa fa-times" aria-hidden="true"></span></a>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>