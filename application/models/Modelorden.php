<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelorden extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function get($array=null){
		if(!empty($array)){
			$this->db->where($array);
			$query = $this->db->get('orders');
			return $query->row();
		}else{
			$query = $this->db->get('orders');
			return $query->result();
		}
	}

	function insert($array){
		$this->db->insert('orders', $array);
		return $this->db->insert_id();
	}

	function insertinner($array){
		$this->db->insert('orders-detail', $array);
	}

	function getmax(){
		$this->db->select_max('num');
		$query = $this->db->get('orders');
		return $query->row();
	}

	function update($array,$id){
		unset($array['id']);
		$this->db->where($id);
		return $this->db->update('products',$array);
	}

	function delete($id){
		$this->db->where($id);
		return $this->db->delete('products');
	}

}