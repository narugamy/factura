$(document).on('ready', function () {

	var base_url = window.location.origin;
	Eventajax('#wrapper','submit','.contenido .form','json','action');
	Eventajax('#wrapper','click','.contenido .boton','json','href');
	Eventajax('#wrapper','click','.contenido .boton2','html','href');
	Eventajax('#wrapper','click','.contenido .banner a','html','href');
	Eventajax('#wrapper','click','.menu-left li a','html','href');
	Eventajax('#wrapper','click','.contenido .text a','html','href');
	initControls();
	setInterval(classes, 500);
	$('html').niceScroll({styler: "fb", cursorcolor: "#1ABC9C", cursorwidth: '8', cursorborderradius: '10px', background: '#f3f3f4', spacebarenabled: false, cursorborder: '0', zindex: '10000'});
	$('html').getNiceScroll();
	if($("html").hasClass('scrollbar1-collapsed')){
		$("html").getNiceScroll().hide();
	}

	function classes() {
		if($(window).width() < 769){
			$("nav.navbar").removeClass('navbar-fixed-top');
			$(".contenedor_principal").css('margin-top', '-1.3em');
		}else{
			$("nav.navbar").addClass('navbar-fixed-top');
			$(".contenedor_principal").css('margin-top', '3.7em');
		}
	}

	function Eventajax(contenedor, evento, disparo,type,tipo){
		$(contenedor).on(evento, disparo, function (event){
			var web=$(this).attr(tipo);
			var datos;
			if(tipo!='action'){
				datos={stado: 1};
			}else{
				datos=$(this).serialize();
			}
			event.preventDefault();
			fnc(web,datos,type);
		});
	}

	function fnc(web,datos,type){
		$.post(web, datos, null, type)
		.done(function (dato){
			if(type == 'json'){
				if(dato.exito){
					if(dato.url!=base_url+"/paneluser" && dato.url!=base_url+"/destroy"){
						fnc(dato.url, {alert: dato.alert, alertc: dato.alertc, stado: 1}, 'html');
						window.history.pushState(null,"", dato.url);
						initControls();
					}else{
						window.location.href=dato.url;
					}
				}else{
					validaciones(dato);
				}
			}else{
				window.history.pushState(null,"", web);
				$('.contenido').html(dato);
				initControls();
			}
		})
		.error(function(){
			alert("Error en el proceso");
		});
	}

	function initControls(){
		window.location.hash="red";
		window.location.hash="Red" //chrome
		window.onhashchange=function(){
			window.location.hash="red";
		}
	}

	function validar(id,valor){
		if(valor){
			$('#'+id).html(valor);
			$('#'+id).addClass("alert alert-danger");
		}else{
			$('#'+id).text("");
			$('#'+id).removeClass();
		}
	}

	function validaciones(dato){
		if(typeof(dato.errores.name)!='undefined'){
			validar('nam',dato.errores.name,"alert alert-danger");}
		if(typeof(dato.errores.phone)!='undefined'){
			validar('pho',dato.errores.phone,"alert alert-danger");}
		if(typeof(dato.errores.message)!='undefined'){
			validar('text',dato.errores.message,"alert alert-danger");}
		if(typeof(dato.errores.apellidos)!='undefined'){
			validar('ape',dato.errores.apellidos,"alert alert-danger");}
		if(typeof(dato.errores.email)!='undefined'){
			validar('ema',dato.errores.email,"alert alert-danger");}
		if(typeof(dato.errores.password)!='undefined'){
			validar('pas',dato.errores.password,"alert alert-danger");}
		if(typeof(dato.errores.nacimiento)!='undefined'){
			validar('dat',dato.errores.date,"alert alert-danger");}
		if(typeof(dato.errores.image_id)!='undefined'){
			validar('fil',dato.errores.image_id,"alert alert-danger");}
		if(typeof(dato.general)!='undefined'){
			validar('gen',dato.general,"alert alert-danger");}
	}

});